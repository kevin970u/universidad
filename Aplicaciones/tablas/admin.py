from django.contrib import admin
from .models import carreraKACA
from .models import cursoKACA
from .models import asignaturaKACA

# Register your models here.

admin.site.register(carreraKACA)
admin.site.register(cursoKACA)
admin.site.register(asignaturaKACA)