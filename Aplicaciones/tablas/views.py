from django.shortcuts import render, redirect
from .models import carreraKACA, cursoKACA, asignaturaKACA
from django.contrib import messages
from django.conf import settings
from django.core.mail import send_mail
from django.http import HttpResponseRedirect

# Create your views here.
def index(request):
	return render(request,'index.html')



def listadoCarrera(request):
	carreraBdd = carreraKACA.objects.all()
	return render(request, 'listadoCarrera.html', {'carrera': carreraBdd})

def guardarCarrera(request):
	nombreCarreraKACA = request.POST["nombreCarreraKACA"]
	directorCarreraKACA = request.POST["directorCarreraKACA"]
	logoCarreraKACA = request.FILES.get("logoCarreraKACA")
	facultadCarreraKACA = request.POST["facultadCarreraKACA"]
	descripcionCarreraKACA = request.POST["descripcionCarreraKACA"]

	nuevoCarrera = carreraKACA.objects.create(nombreCarreraKACA=nombreCarreraKACA, directorCarreraKACA=directorCarreraKACA, logoCarreraKACA=logoCarreraKACA, facultadCarreraKACA=facultadCarreraKACA, descripcionCarreraKACA=descripcionCarreraKACA)
	messages.success(request, 'Carrera guardada Exitosamente')
	return redirect('/listadoCarrera')

def eliminarCarrera(request, idCarreraKACA):
	carreraEliminar = carreraKACA.objects.get(idCarreraKACA=idCarreraKACA)
	carreraEliminar.delete()
	messages.success(request, 'Carrera eliminada Exitosamente')
	return redirect('/listadoCarrera')

def editarCarrera(request, idCarreraKACA):
	carreraEditar = carreraKACA.objects.get(idCarreraKACA=idCarreraKACA)
	return render(request, 'editarCarrera.html', {'carrera': carreraEditar})

def procesarActualizacionCarrera(request):
	idCarreraKACA = request.POST["idCarreraKACA"]
	nombreCarreraKACA = request.POST["nombreCarreraKACA"]
	directorCarreraKACA = request.POST["directorCarreraKACA"]
	logoCarreraKACA = request.FILES.get("logoCarreraKACA")
	facultadCarreraKACA = request.POST["facultadCarreraKACA"]
	descripcionCarreraKACA = request.POST["descripcionCarreraKACA"]

	carreraEditar = carreraKACA.objects.get(idCarreraKACA=idCarreraKACA)
	carreraEditar.nombreCarreraKACA = nombreCarreraKACA
	carreraEditar.directorCarreraKACA = directorCarreraKACA
	carreraEditar.logoCarreraKACA = logoCarreraKACA
	carreraEditar.facultadCarreraKACA = facultadCarreraKACA
	carreraEditar.descripcionCarreraKACA = descripcionCarreraKACA
	carreraEditar.save()
	messages.success(request, 'Carrera actualizada Exitosamente')
	return redirect('/listadoCarrera')





def listadoCurso(request):
	cursoBdd = cursoKACA.objects.all()
	carreraBdd = carreraKACA.objects.all()
	return render(request, 'listadoCurso.html', {'curso': cursoBdd, 'carrera': carreraBdd})

def guardarCurso(request):
	idCarreraKACA = request.POST["idCarreraKACA"]
	carreraSeleccionado = carreraKACA.objects.get(idCarreraKACA=idCarreraKACA)
	nivelCursoKACA = request.POST["nivelCursoKACA"]
	descripcionCursoKACA = request.POST["descripcionCursoKACA"]
	aulaCursoKACA = request.POST["aulaCursoKACA"]
	numeromesasCursoKACA = request.POST["numeromesasCursoKACA"]
	bloqueCursoKACA = request.POST["bloqueCursoKACA"]

	nuevoCurso = cursoKACA.objects.create(nivelCursoKACA=nivelCursoKACA, descripcionCursoKACA=descripcionCursoKACA, aulaCursoKACA=aulaCursoKACA, numeromesasCursoKACA=numeromesasCursoKACA, bloqueCursoKACA=bloqueCursoKACA, carrera=carreraSeleccionado)
	messages.success(request, 'Curso guardado Exitosamente')
	return redirect('/listadoCurso')

def eliminarCurso(request, idCursoKACA):
	cursoEliminar = cursoKACA.objects.get(idCursoKACA=idCursoKACA)
	cursoEliminar.delete()
	messages.success(request, 'Curso eliminado Exitosamente')
	return redirect('/listadoCurso')

def editarCurso(request, idCursoKACA):
	cursoEditar = cursoKACA.objects.get(idCursoKACA=idCursoKACA)
	carreraBdd = carreraKACA.objects.all()
	return render(request, 'editarCurso.html', {'curso': cursoEditar, 'carrera': carreraBdd})

def procesarActualizacionCurso(request):
	idCursoKACA = request.POST["idCursoKACA"]
	idCarreraKACA = request.POST["idCarreraKACA"]
	carreraSeleccionado = carreraKACA.objects.get(idCarreraKACA=idCarreraKACA)
	nivelCursoKACA = request.POST["nivelCursoKACA"]
	descripcionCursoKACA = request.POST["descripcionCursoKACA"]
	aulaCursoKACA = request.POST["aulaCursoKACA"]
	numeromesasCursoKACA = request.POST["numeromesasCursoKACA"]
	bloqueCursoKACA = request.POST["bloqueCursoKACA"]

	cursoEditar = cursoKACA.objects.get(idCursoKACA=idCursoKACA)
	cursoEditar.carrera = carreraSeleccionado
	cursoEditar.nivelCursoKACA = nivelCursoKACA
	cursoEditar.descripcionCursoKACA = descripcionCursoKACA
	cursoEditar.aulaCursoKACA = aulaCursoKACA
	cursoEditar.numeromesasCursoKACA = numeromesasCursoKACA
	cursoEditar.bloqueCursoKACA = bloqueCursoKACA
	cursoEditar.save()
	messages.success(request, 'Curso actualizado Exitosamente')
	return redirect('/listadoCurso')






def listadoAsignatura(request):
	asignaturaBdd = asignaturaKACA.objects.all()
	cursoBdd = cursoKACA.objects.all()
	return render(request, 'listadoAsignatura.html', {'asignatura': asignaturaBdd, 'curso': cursoBdd})

def guardarAsignatura(request):
	idCursoKACA = request.POST["idCursoKACA"]
	cursoSeleccionado = cursoKACA.objects.get(idCursoKACA=idCursoKACA)
	nombreAsignaturaKACA = request.POST["nombreAsignaturaKACA"]
	creditosAsignaturaKACA = request.POST["creditosAsignaturaKACA"]
	fechaInicioAsignaturaKACA = request.POST["fechaInicioAsignaturaKACA"]
	fechaFinalizacionAsignaturaKACA = request.POST["fechaFinalizacionAsignaturaKACA"]
	profesorAsignaturaKACA = request.POST["profesorAsignaturaKACA"]
	silaboAsignaturaKACA = request.FILES.get("silaboAsignaturaKACA")

	nuevoAsignatura = asignaturaKACA.objects.create(nombreAsignaturaKACA=nombreAsignaturaKACA, creditosAsignaturaKACA=creditosAsignaturaKACA, fechaInicioAsignaturaKACA=fechaInicioAsignaturaKACA, fechaFinalizacionAsignaturaKACA=fechaFinalizacionAsignaturaKACA, profesorAsignaturaKACA=profesorAsignaturaKACA, silaboAsignaturaKACA=silaboAsignaturaKACA, curso=cursoSeleccionado)
	messages.success(request, 'Asignatura guardada Exitosamente')
	return redirect('/listadoAsignatura')

def eliminarAsignatura(request, idAsignaturaKACA):
	asignaturaEliminar = asignaturaKACA.objects.get(idAsignaturaKACA=idAsignaturaKACA)
	asignaturaEliminar.delete()
	messages.success(request, 'Asignatura eliminada Exitosamente')
	return redirect('/listadoAsignatura')

def editarAsignatura(request, idAsignaturaKACA):
	asignaturaEditar = asignaturaKACA.objects.get(idAsignaturaKACA=idAsignaturaKACA)
	cursoBdd = cursoKACA.objects.all()
	return render(request, 'editarAsignatura.html', {'asignatura': asignaturaEditar, 'curso': cursoBdd})

def procesarActualizacionAsignatura(request):
	idAsignaturaKACA = request.POST["idAsignaturaKACA"]
	idCursoKACA = request.POST["idCursoKACA"]
	cursoSeleccionado = cursoKACA.objects.get(idCursoKACA=idCursoKACA)
	nombreAsignaturaKACA = request.POST["nombreAsignaturaKACA"]
	creditosAsignaturaKACA = request.POST["creditosAsignaturaKACA"]
	fechaInicioAsignaturaKACA = request.POST["fechaInicioAsignaturaKACA"]
	fechaFinalizacionAsignaturaKACA = request.POST["fechaFinalizacionAsignaturaKACA"]
	profesorAsignaturaKACA = request.POST["profesorAsignaturaKACA"]
	silaboAsignaturaKACA = request.FILES.get("silaboAsignaturaKACA")

	asignaturaEditar = asignaturaKACA.objects.get(idAsignaturaKACA=idAsignaturaKACA)
	asignaturaEditar.curso = cursoSeleccionado
	asignaturaEditar.nombreAsignaturaKACA = nombreAsignaturaKACA
	asignaturaEditar.creditosAsignaturaKACA = creditosAsignaturaKACA
	asignaturaEditar.fechaInicioAsignaturaKACA = fechaInicioAsignaturaKACA
	asignaturaEditar.fechaFinalizacionAsignaturaKACA = fechaFinalizacionAsignaturaKACA
	asignaturaEditar.profesorAsignaturaKACA = profesorAsignaturaKACA
	asignaturaEditar.silaboAsignaturaKACA = silaboAsignaturaKACA
	asignaturaEditar.save()
	messages.success(request, 'Asignatura actualizada Exitosamente')
	return redirect('/listadoAsignatura')


def vista1(request):
	return render(request, 'enviar_correo.html')

def enviar_correo(request):
	if request.method == 'POST':
		destinatario = request.POST.get('destinatario')
		asunto = request.POST.get('asunto')
		cuerpo = request.POST.get('cuerpo')

		send_mail(asunto, cuerpo, settings.EMAIL_HOST_USER, [destinatario], fail_silently=False)

		messages.success(request, 'Se ha enviado tu correo ')
		return HttpResponseRedirect('/vista1')
	
	return render(request, ' enviar_correo.html')