from django.db import models

# Create your models here.

from django.db import models

# Create your models here.

class carreraKACA(models.Model):
    idCarreraKACA = models.AutoField(primary_key=True)
    nombreCarreraKACA = models.CharField(max_length=150)
    directorCarreraKACA = models.CharField(max_length=150)
    logoCarreraKACA = models.FileField(upload_to='logos',null=True,blank=True)
    facultadCarreraKACA = models.CharField(max_length=150)
    descripcionCarreraKACA = models.TextField()

    def __str__(self):
        fila = "{0}: {1} {2}"
        return fila.format(self.idCarreraKACA,self.nombreCarreraKACA, self.directorCarreraKACA)

class cursoKACA(models.Model):
    idCursoKACA = models.AutoField(primary_key=True)
    nivelCursoKACA = models.CharField(max_length=150)
    descripcionCursoKACA = models.TextField()
    aulaCursoKACA = models.CharField(max_length=150)
    numeromesasCursoKACA = models.CharField(max_length=150)
    bloqueCursoKACA = models.CharField(max_length=150)
    carreraKACA = models.ForeignKey(carreraKACA,null=True,blank=True,on_delete=models.PROTECT)
    
    def __str__(self):
        fila = "{0}: {1} {2}"
        return fila.format(self.idCursoKACA,self.nivelCursoKACA, self.descripcionCursoKACA)

class asignaturaKACA(models.Model):
    idAsignaturaKACA = models.AutoField(primary_key=True)
    nombreAsignaturaKACA = models.CharField(max_length=150)
    creditosAsignaturaKACA = models.CharField(max_length=150)
    fechaInicioAsignaturaKACA = models.DateField()
    fechaFinalizacionAsignaturaKACA = models.DateField()
    profesorAsignaturaKACA = models.CharField(max_length=150)
    silaboAsignaturaKACA = models.FileField(upload_to='silabos',null=True,blank=True)
    cursoKACA = models.ForeignKey(cursoKACA,null=True,blank=True,on_delete=models.PROTECT)
    
    def __str__(self):
        fila = "{0}: {1} {2}"
        return fila.format(self.idAsignaturaKACA,self.nombreAsignaturaKACA, self.profesorAsignaturaKACA)
