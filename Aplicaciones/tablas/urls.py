from django.urls import path
from . import views

urlpatterns=[  
	path('',views.index),
	path('listadoCarrera',views.listadoCarrera, name="listadoCarrera"),
	path('guardarCarrera/',views.guardarCarrera),
	path('eliminarCarrera/<idCarreraKACA>',views.eliminarCarrera),
	path('editarCarrera/<idCarreraKACA>',views.editarCarrera),
	path('procesarActualizacionCarrera/',views.procesarActualizacionCarrera),
	path('listadoCurso',views.listadoCurso, name="listadoCurso"),
	path('guardarCurso/',views.guardarCurso),
	path('eliminarCurso/<idCursoKACA>',views.eliminarCurso),
	path('editarCurso/<idCursoKACA>',views.editarCurso),
	path('procesarActualizacionCurso/',views.procesarActualizacionCurso),
	path('listadoAsignatura',views.listadoAsignatura, name="listadoAsignatura"),
	path('guardarAsignatura/',views.guardarAsignatura),
	path('eliminarAsignatura/<idAsignaturaKACA>',views.eliminarAsignatura),
	path('editarAsignatura/<idAsignaturaKACA>',views.editarAsignatura),
	path('procesarActualizacionAsignatura/',views.procesarActualizacionAsignatura),
    path('vista1/', views.vista1, name='vista1'),
    path('enviar-correo/', views.enviar_correo, name='enviar_correo')


]